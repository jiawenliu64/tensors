Tensors generated from [Frostt](http://frostt.io/tensors/):

chicago, delicious, flickr-4d, nell-2, nips, uber, vast

Tensors generated from [NWChem](https://www.nwchem-sw.org/):

uracil_trimer.sto-3g_T2

Tensors generated from [ITensor](https://itensor.org/):

tensor_2137, tensor_2143, tensor_2151, tensor_2163, tensor_2164, tensor_2169, tensor_2170, tensor_2177, tensor_2178, tensor_2190
